#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <omp.h>

int main(){
	int n = 8, i=0, j=0, count=0;
	FILE *f;
        char c, str[20], file_name[20];
	char ** words;

	words = (char **) malloc(n*sizeof(char *));
        words[j] = (char *) malloc(100*sizeof(char));

  //removed & from variables
	scanf("%s\n%s", file_name, str);
	
	//Open the file
	f = fopen(file_name, "r");
	if(f == NULL){
		printf("Cannot open file.\n");
		exit(0);
	}
	c = getc(f);

	/*
	//Create linked list
	while(c != EOF){		
		if(c == '\0' || c == '\b' || c == ' ' || c == '\t' || c == '\n' ){
			if(j >= n){
				n *= 2;
				words = (char **) realloc(words, n*sizeof(char *));
			}
			//why is that necessary?	
			for(i=i; i<15; i++)
				words[j][i] = '\0';
	
			i=0;
			j++;
			//if j==n you are acessing an invalid memory area
			words[j] = (char *) malloc(100*sizeof(char));
		}else{
			words[j][i] = c;
			i++;
		}

		c = getc(f);
	}
	*/

	//Create linked list
	i=0;
	while(c != EOF){
		while(c == '\b' || c == ' ' || c == '\t' || c == '\n' ){
			c = getc(f);
		}
		while(i<99 && c != EOF &&	c != '\0' && c != '\b' && c != ' ' && c != '\t' && c != '\n' ){
			words[j][i++] = c;
			c = getc(f);
		}
		words[j][i] = '\0';
		if(c != EOF){
			j++;
			if(j == n){
				n *= 2;
				words = (char **) realloc(words, n*sizeof(char *));
			}
			words[j] = (char *) malloc(100*sizeof(char));
			i=0;
		}
	}


	double t_start = omp_get_wtime();
  
	//Parallelize here
	for(i=0; i<j; i++){
		if(strcmp(str, words[i]) == 0){		
			count++;
		}

	} 

	double t_end = omp_get_wtime();

	//include to deallocate memory
	for(i=0; i<=j; i++)
		free(words[i]);
	free(words);

	
	printf("%s: %d occurrences of %d\n", str, count, j);
	printf("Time: %lf\n", t_end - t_start);   
  	
  	fclose(f);
}


 
